<?php


/**
 * @todo Move out all classes in separate file and implement a autoloading
 * @todo Split ParseByKeywords action to some components or etc
 * @todo Refactor configs implementation
 * @todo Add more checkings on errors
 * @todo Implement some standardized command runner to process various scenarios for this app
 */


/**
 * Our App should have those basic methods
 */
interface IApp {

    public function run();
    public function configure($config);
}

/**
 * A basic methods for app components
 */
interface IAppComponent
{
    public function init();
    public function getIsInitialized();
}


/**
 * A helper class to operate with app loading
 */
class PR {

    const DEFAULT_APP_TYPE = 'PRApp';

    private static $_app;

    /**
     * Returns a application specific type instance, defined by configuration
     * @param null $config
     * @param string $class a type of application
     * @return mixed|IApp
     */
    public static function createApplication($config = null, $class)
    {
        return new $class($config, strtolower($class));
    }


    /**
     * Import external classes
     * @param $type
     */
    public static function import($type) {
        //here we could run some autoloader
    }

    /**
     * @param $config
     * @param string $class
     * @return CoreApp
     */
    public static function getApp($config, $class = self::DEFAULT_APP_TYPE)
    {
        if(self::$_app === null)
            self::$_app = self::createApplication($config, $class);

        return self::$_app;
//        else
//            throw new Exception('An application can only be created once!');
    }

}

/**
 * This is a basic class to extend from within some useful features
 */
abstract class CoreUnit {

    private $_id = null;

    private $_members;

    public function __construct($config = array(), $id = null) {

        //some preparations...

        if (isset($id)) {
            $this->_id = (string)$id;
        } else {
            if (isset($config['class'])) {
                $this->_id = strtolower($config['class']);
            }
            else {
                throw new Exception('Unit should have identifier!');
            }
        }
//        var_dump($id);
//        var_dump($config);

        $this->preInit();
        $this->configure($config);
        //other stuff....
        $this->init();
    }

    public function __get($name)
    {
        //@todo add checks
        return $this->_members[$name];
    }

    public function __set($name, $value)
    {
        //@todo checks before save
        $this->_members[$name] = $value;
    }

    public function has($name)
    {
        return isset($this->_members[$name]);
        //return method_exists($this,'get'.$name) || method_exists($this,'set'.$name);
    }

    /**
     * Configures the module with the specified configuration.
     * @param array $config the configuration array
     */
    public function configure($config)
    {
        if(is_array($config))
        {
            foreach($config as $key=>$value)
                $this->$key=$value;
        }
    }

    public function fireEvent($name, $event) {
        //
    }


    protected function preInit()
    {

    }

    protected function init()
    {

    }

}



abstract class CoreApp extends CoreUnit implements IApp {

    /**
     * A storage for each components configs
     * @var array
     */
    private $_cMembers = array();

    /**
     * Contains components instances
     * @var array
     */
    private $_components = array();

    /**
     * Get component by identifier (same as key in config, at 'components' section)
     * @param $id
     * @return mixed
     */
    public function getComponent($id) {
        if (isset($this->_components[$id])) {
            return $this->_components[$id];
        } else {
            $this->_components[$id] = $this->createComponent($this->getComponentConfig($id));
            $this->_components[$id]->init();
        }

        return $this->_components[$id];
    }

    /**
     * Returns an instance of component specified in given config
     * @param $config
     * @return mixed
     * @throws Exception
     */
    public function createComponent($config) {
        //var_dump($config);
        if (is_string($config)) {
            $type = $config;
            $config = array();
        } elseif (isset($config['class'])) {
            $type = $config['class'];
            //unset($config['class']);
        } else
            throw new Exception('Object configuration must be an array containing a "class" element.');
        if (!class_exists($type, false))
            $type = PR::import($type);

        $object = new $type($config);
        foreach ($config as $key => $value)
            $object->$key = $value;
        return $object;

    }

    /**
     * Set component into local register
     * @param $id
     * @param $component
     * @return $this
     */
    public function setComponent($id, $component) {

        if ($component === null) {
            unset($this->_components[$id]);
            return;
        } elseif ($component instanceof IAppComponent) {
            $this->_components[$id] = $component;
            //run init if it was not initialized yet
            if (!$component->getIsInitialized())
                $component->init();
        }

        /**
         * We are only setting a config for component, real loading will be performed later (aka lazy loading)
         * @see $this->getComponent($id)
         */
        if (is_array($component)) {
            $this->_cMembers[$id] = $component;
        }

        return $this;
    }

    /**
     * Returns a configuration array for specific component
     * @param $id
     * @return array
     */
    public function getComponentConfig($id) {

        //var_dump($this->_cMembers);
        $c = array();
        if (isset($this->_cMembers[$id])) {
            $c = $this->_cMembers[$id];
        }
        return $c;
    }


    /**
     * Processes the request.
     * This is the place where the actual request processing work is done.
     * Derived classes should override this method.
     */
    abstract public function processRequest();

    /**
     * Run the app and process a needed bunch of code
     */
    public function run()
    {
        //fireEvent beginRequest
        $this->processRequest();
        //fireEvent endRequest
    }


    protected function init() {
        //preconfiguring components if config was passed while creating
        if ($this->has('components')) {
            foreach ($this->components as $cId => $cConfig) {
                $this->setComponent($cId, $cConfig);
            }
        }
    }
}


/**
 * A Base db DAO class
 * Requires PDO
 */
class BaseDb extends CoreUnit implements IAppComponent {

    private $_isInitialized = false;

    /**
     * @var null|PDO
     */
    private $_pdo = null;

    private $_isActive = false;

    public function init() {
        //e.g. if is autoConnect = true -> do the connection
        $this->_isInitialized = true;

        if ($this->autoConnect) {
            $this->getConnection();
        }
    }

    public function getIsInitialized() {
        return $this->_isInitialized;
    }

    /**
     * Initialize given DO
     * @param $pdo
     */
    protected function initConnection($pdo) {
        /**
         * @var $pdo PDO
         */
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //set persistence
        $pdo->setAttribute(PDO::ATTR_PERSISTENT, false);

        if(constant('PDO::ATTR_EMULATE_PREPARES'))
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

        if ($this->charset !== null) {
            $driver=strtolower($pdo->getAttribute(PDO::ATTR_DRIVER_NAME));
            // for mysql only, unknown for other RDBMS's if it supported
            if($driver == 'mysql')
                $pdo->exec('SET NAMES '.$pdo->quote($this->charset));
        }

        //just for test
        $pdo->query('SHOW TABLES')->fetchAll(PDO::FETCH_ASSOC);

    }

    /**
     * Returns PDO connected instance
     *
     * @return null|PDO
     * @throws Exception
     */
    public function getConnection() {
        if($this->_pdo===null)
        {
            if(!$this->has('connectionString'))
                throw new Exception('Required connectionString cannot be empty.');
            try
            {

                $pdoClass = 'PDO';
                if (($pos = strpos($this->connectionString, ':')) !== false) {
                    $driver = strtolower(substr($this->connectionString, 0, $pos));

                    switch ($driver) {
                        case 'mssql';
                            $pdoClass = 'CMssqlPdoAdapter';
                        break;

                        default:
                            $pdoClass = $pdoClass;
                        break;
                    }

                    if(!class_exists($pdoClass))
                        throw new Exception('BaseDb is unable to find PDO class. Make sure PDO is installed correctly');

                    @$instance = new $pdoClass($this->connectionString, $this->username, $this->password);

                    if(!$instance)
                        throw new Exception('Failed opening db connection=(((');

                }

                /**
                 * @var $instance PDO
                 */

                $this->_pdo = $instance;

                $this->initConnection($this->_pdo);
                $this->_isActive = true;
            }
            catch(PDOException $e)
            {
                throw new Exception('BaseDb failed to open the DB connection: '.
                $e->getMessage() . '. Code: '. (int)$e->getCode(). '. Info: '. $e->errorInfo);
            }
        }

        return $this->_pdo;
    }

    /**
     * Performs an insert query
     * @param $table
     * @param $columns array of key=>value
     * @return bool whether query was successful or not
     */
    public function insert($table, $columns)
    {
        $params = array();
        $names = array();
        $placeholders = array();

        foreach ($columns as $name => $value) {
            $names[] = '`' . $name . '`';

            $placeholders[] = ':' . $name;
            $params[':' . $name] = $value;
        }

        $sql = 'INSERT INTO ' . '`' . $table . '`'
            . ' (' . implode(', ', $names) . ') VALUES ('
            . implode(', ', $placeholders) . ')';

        return $this->getConnection()->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY))->execute($params);

    }


    public function __destruct()
    {
        $this->_pdo = null;
        $this->_isActive = false;
    }

}


/**
 * A Simple URL Parser and CURL Wrapper within
 *
 * Class PRParser
 */
class PRParser extends CoreUnit implements IAppComponent {

    private $_isInitialized = false;

    protected $_ch;

    /**
     * Default CURL configuration
     * @var array
     */
    protected $_cc = array(
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 1000,
        CURLOPT_TIMEOUT => 100,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20110619 Firefox/5.0'
    );

    const DEFAULT_URL = "http://rozetka.com.ua/search";

    /**
     * Setting specific CURL option
     * @param $option
     * @param $value
     * @return $this
     */
    public function setOption($option, $value)
    {
        curl_setopt($this->_ch, $option, $value);
        return $this;
    }

    /**
     * Setting specific CURL options
     * @param array $options
     * @return $this
     */
    public function setOptions($options = array())
    {
        curl_setopt_array($this->_ch, $options);

        return $this;
    }


    /**
     * Doing CURL initialization
     * @throws Exception
     */
    public function init() {

        $this->parseUrl = self::DEFAULT_URL;

        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '128M');

        set_time_limit(0);

        try{
            $this->_ch = curl_init();
            $options = $this->_cc;
            $this->setOptions($options);

            $ch = $this->_ch;

            $this->_isInitialized = true;

        }catch(Exception $e){
            throw new Exception('Curl not installed');
        }

    }

    public function getIsInitialized() {
        return $this->_isInitialized;
    }

    /**
     * We are not forgetting closing a session
     */
    public function __destruct() {
        if (isset($this->_ch)) {
            curl_close($this->_ch);
        }
    }

    /**
     * Curl exec wrapper
     * @param $url
     * @return bool|mixed
     * @throws Exception
     */
    private function _exec($url){

        $this->setOption(CURLOPT_URL, $url);
        $c = curl_exec($this->_ch);
        if(!curl_errno($this->_ch))
            return $c;
        else
            throw new Exception(curl_error($this->_ch));

    }

    /**
     * Returns errors caused during request
     * @return string
     */
    public function getError()
    {
        return curl_error($this->_ch);
    }

    /**
     * HTTP GET
     * @param $url
     * @return bool|mixed
     */
    public function get($url) {
        $this->setOption(CURLOPT_HTTPGET, true);
        return $this->_exec($url);
    }


    public function setParseURL($url) {
        $this->$this->parseUrl;
    }

    public function getParseURL() {

    }

    /**
     * Multidimensional sorting by specified column
     * @param $arr
     * @param $col
     * @param int $dir
     */
    public function arraySortByColumn(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

}


class PRApp extends CoreApp {


    protected  function init() {
        //init commands(ations) from config
        parent::init();
    }


    /**
     * Fetching a needed action with params to run
     * @param $args
     * @return array
     */
    protected function resolveRequest($args)
    {
        $params = array();
        foreach ($args as $arg) {
            if (isset($action))
                $params[] = $arg;
            else
                $action = $arg;
        }
        if (!isset($action))
            $action = 'index'; //default action

        $paramsOut = array();
        foreach ($params as $param) {
            parse_str($param, $parsed);
            $paramsOut = array_merge($paramsOut, $parsed);
        }

        return array($action, $paramsOut);
    }

    /**
     * Processing a request with given args
     */
    public function processRequest() {
        $args = $_SERVER['argv'];

        $scriptName=$args[0];
        array_shift($args);
        if(isset($args[0]))
        {
            $name=$args[0];
            array_shift($args);
        }

        //var_dump($args);

        list($action, $args) = $this->resolveRequest($args);
        //var_dump($action, $args);

        //a kind of router/controller
        switch(strtolower($action)) {
            case 'parseurl':
                echo "parse url....\r\n";
                break;

            case 'bykeywords':
                $this->actionParseByKeywords($args);
                break;

            case 'index':

            default:
                echo "Om nom nom...\r\n";
            break;
        }

    }


    /**
     *
     * @param $args
     */
    public function actionParseByKeywords($args) {

        $keywords = implode(',', array_keys($args));

        $url = $this->getComponent('parser')->parseUrl . '/?text='. $keywords;

        $result = $this->getComponent('parser')->get($url);

        ///disabling warnings
        libxml_use_internal_errors(true);

        $doc = new DOMDocument();
        @$doc->loadHTML($result);
        //$doc->preserveWhiteSpace = false;


        $finder = new DOMXPath($doc);

        $patterns['name'] = ".//td[@class='detail']/div[@class='title']/a/text()";

        $patterns['price'] = ".//td[@class='price-status']/div[@name='prices_active_element_original']/div[@name='price']/div[@class='uah']/text()";

        $patterns['link'] = ".//td[@class='detail']/div[@class='title']/a/@href";


        $nodes = $finder->query(
            "//*[contains(concat(' ', normalize-space(@class), ' '), ' item available action ')]"
        );


        /**
         * @var $node DOMNode
         */
        $i = 0;
        $fetchedData = array();
        foreach ($nodes as $id => $node) {

            foreach($patterns as $key => $pattern) {

                $moreNode = $finder->query($pattern, $node);

                foreach ($moreNode as $n) {
                    $fetchedData[$id][$key] = mysql_real_escape_string(trim(utf8_decode($n->nodeValue)));
                    //break;
                }
                //break;

                if ($i>=10000) //max
                    break;
                $i++;

            }


        }


        /**
         * Preparing array for csv encoding
         */
        $fetchedDataPrepared = array();
        $fetchedDataPrepared[] = array_keys($patterns);


        $this->getComponent('parser')->arraySortByColumn($fetchedData, 'price', SORT_ASC);

        foreach ($fetchedData as $fetchedDataItem) {
            $data = array_values($fetchedDataItem);
            $fetchedDataPrepared[] = $data;
        }

        // output up to 5MB is kept in memory, if it becomes bigger
        // it will automatically be written to a temporary file
        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');

        foreach($fetchedDataPrepared as $fields) {
            fputcsv($csv, $fields, ',', '"');
        }

        rewind($csv);

        // put it all in a variable
        $csvString = stream_get_contents($csv);

        fclose($csv);

        $insertData = array(
            'id' => null,
            'query' => $keywords,
            'fetched_csv' => $csvString,
            'date' => time()
        );

        //saving data into db
        if ($this->getComponent('db')->insert('parse_rozetka', $insertData)) {
            echo "Data has been successfully saved!\r\n";
        }else {
            echo "Smth went wrong...\r\nDetails:\r\n";
            var_dump($this->getComponent('db')->getConnection()->errorInfo());
        }

    }

}


/**
 * Configuration
 */
$config = array(
    'components' => array(
        'db' => array(
            'class' => 'BaseDb',
            'connectionString' => 'mysql:host=localhost;dbname=ilogos_dev;',
            'username' => 'ilogos_dev',
            'password' => 'bazinga',
            'charset' => 'utf8',
            'autoConnect' => false
        ),
        'commandRunner' => array(
            'commands' => array(
                'test' => array(),
                'parseRozetka' => array(
                    'defaultParseURL' => '',
                    'enabled' => true
                )
            )
        ),
        'parser' => array(
            'class' => 'PRParser',
            'defaultParseURL' => 'http://rozetka.com.ua/search'
        ),
    )
);


/**
 * Main entering point
 */
PR::getApp($config)->run();

