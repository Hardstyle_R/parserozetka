
# Simple CLI URL Parser

## Requirements
* PHP 5.3+
* Curl and php-curl installed
* DOM Extension with libxml
* PDO Extension

## Setup
* To run an application pass an array of configuration:

```php

$config = array(
    'components' => array(
        'db' => array(
            'class' => 'BaseDb',
            'connectionString' => 'mysql:host=<your host>;dbname=<your database name>;',
            'username' => '<your username>',
            'password' => '<your password>',
            'charset' => 'utf8',
        ),
        //...
    )
);

PR::getApp($config)->run();

```

## Usage:

```php -f index.php <command> <action> <params>```

```php -f index.php parseurl ByKeywords фотоаппарат```

