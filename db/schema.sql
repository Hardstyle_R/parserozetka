
DROP TABLE IF EXISTS `parse_rozetka`;

CREATE TABLE `parse_rozetka` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(1024) DEFAULT NULL,
  `fetched_csv` text,
  `date` int(11) unsigned DEFAULT NULL COMMENT 'Timestamp int value',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- test data row
INSERT INTO `parse_rozetka` (`id`, `query`, `fetched_csv`, `date`)
VALUES
	(
    NULL,'фотоаппарат','name,price,link\n\"Сумка Vanguard UP-Rise 15 NEW\",363,http://rozetka.com.ua/39663/p39663/\n\"Рюкзак Vanguard BIIN 47 Orange\",388,http://rozetka.com.ua/ryukzak_vanguard_biin_47_orange/p168111/\n\"Сумка Lowepro Nova 140 Black AW (5537010)\",388,http://rozetka.com.ua/lowepro_nova_140_black_aw/p88351/\n\"Сумка для фототехники Vanguard 2GO 32\",487,http://rozetka.com.ua/vangurad_2go_32/p264033/\n\"Рюкзак для фототехники Vanguard 2GO 46\",792,http://rozetka.com.ua/vanguard_2go_46/p263976/\n\"Рюкзак для фототехники Vanguard 2GO 46GR\",792,http://rozetka.com.ua/vanguard_2go_46gr/p263956/\n\"Samsung DV150 (EC-DV150FBPBRU) Black  + чехол + microSDHC 8Gb!\",858,http://rozetka.com.ua/samsung_dv150_ec-dv150fbpbru/p267495/\n\"Samsung DV150 (EC-DV150FBPWRU) White + чехол + microSDHC 8Gb!\",858,http://rozetka.com.ua/samsung_dv150_ec-dv150fbpwru/p267521/\n\"Сумка Vanguard  Quovio 41 Чёрная\",1287,http://rozetka.com.ua/vanguard_quovio_41_black/p292469/\n\"Samsung WB250F Cobalt black (WB250FFPBRU) + SDHC 16Gb!\",1444,http://rozetka.com.ua/samsung_wb_250f_cobalt_black/p263751/\n\"Samsung WB250F White (WB250FFPWRU) + SDHC 16Gb!\",1444,http://rozetka.com.ua/samsung_wb_250f_white/p263705/\n\"Nikon Coolpix L820 Black (VNA330E1) Официальная гарантия! + карта памяти 16гб!\",1815,http://rozetka.com.ua/nikon_coolpix_l820_black_of/p271186/\n\"Samsung NX1100 20-50mm Kit Black + Adobe Photoshop Lightroom 4 + сумка + SDHC 32Gb!\",3044,http://rozetka.com.ua/samsung_nx1100_20_50_black/p347050/\n\"Samsung NX300 18-55mm Kit Black + объектив 45mm F1.8!\",5699,http://rozetka.com.ua/samsung_nx300_18_55mm_kit_black/p265373/\n\"Samsung NX300 18-55mm Kit White + объектив 45mm F1.8!\",5699,http://rozetka.com.ua/samsung_nx300_18_55mm_kit_white/p265359/\n',1384939871
  );

